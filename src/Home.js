import React, { Component } from 'react'
import logo from './logo.svg';
import './App.css';

class Home extends Component {
    state = {
        name: 'Nataniel'
    }

    constructor(props) {
        super(props)
        this.changeName = this.changeName.bind(this)
    }

    changeName = (event) => {
        this.setState({ name: event.target.value })
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Campo</h1>
                    <input type="text" value={this.state.name} onChange={this.changeName} />
                </header>
                <p className="App-intro">
                    Valor do input - {this.state.name}

                </p>
            </div>
        );
    }
}

export default Home
